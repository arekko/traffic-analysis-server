//Global require
require('dotenv').config()
const express = require("express");
const http = require("http");
const bodyParser = require("body-parser");

//Require modules
const request_location = require("./modules/request_location.js");
const request_nav = require("./modules/request_nav.js");
const register = require("./modules/register.js");
const status = require("./modules/status.js");
const navigate = require("./modules/navigate.js");
const init_firebase = require("./modules/init_firebase.js");
const log = require("./modules/log.js");

//Init firebase stuff
const admin = init_firebase();
const db = admin.firestore();
const usersRef = db.collection("users");

//Init app
app = express();
server = http.createServer(app);
// Middleware configuration
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res) => res.render("./public/index.html"));
server.listen(process.env.APP_PORT, () => log.info(`App listening on port ${process.env.APP_PORT}!`));

// Interval for requesting location
setInterval(() => {
  request_location(usersRef, admin);
}, process.env.REQUEST_TIMEOUT_LOCATION);

// Interval for requesting is navigating
setInterval(() => {
  request_nav(usersRef, admin);
}, process.env.REQUEST_TIMEOUT_ISNAV);

// Route for register user to the db
app.post("/register/", (req, res) => {
  register(res,req,usersRef);
});

// Route for setting the status of a user
app.post("/status/", (req, res) => {
  status(res,req,usersRef);
});

// Route for getting data
app.post("/navigate/", (req, res) => {
  navigate(res,req,admin);
});




