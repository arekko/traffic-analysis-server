# Traffium Server side

## Modules : 
- [Init_firebase](#init-firebase)
- [Log](#log)
- [Navigate](#navigate)
- [Register](#register)
- [Status](#status)
- [Request Location](#request-location)
- [Request Nav](#request-nav)

## Config

### Firebase
- Create a Firebase project
- Enable Cloud Firestore by going in Database tab
- Go to project settings > Service account > Generate new private key
- Put the json file in the project
### .env file
You need to set the .env file in the project
```.env
APP_PORT=3000
REQUEST_TIMEOUT_ISNAV=120000
REQUEST_TIMEOUT_LOCATION=20000
CODE_SUCCESS=200
CODE_ERROR=400

# API KEY
TOMTOM_API_KEY=<TOMTOM API KEY>
PATH_FIREBASE_FILE=<RELATIVE PATH TO FIREBASE JSON FILE CONFIG SDK>
```
## Modules
### Init Firebase
- Description : Module for initialise firebase sdk
### Log
- Description : Module for loging in console
### Navigate
- Description : Route for communicate data
- Type : POST
- Address : ```http://ip:port/navigate/```
- Payload : 
```
{
   "userFirebaseToken": "_user_token",
   "distCoord": {
       "lon": "_",
       "lat": "_"
   },
   "sourceCoord": {
       "lon": "_",
       "lat: "_"
   },
   "speed": "_"
}
```
- Message sent if speed in threshold
```
{
    notification: {
        body: gURL
    },
    token: body.userFirebaseToken
}
```
- Response : 
    - Error : TODO
    - Success : 
    ```
    {
        code: CODE_SUCCESS,
        message: "Success"
    }
    ```
### Register
- Desciption : Register the token to the server
- Type : POST
- Address : ```http://ip:port/register/```
- Payload : 
```
{
    “registrationToken”: “_user_token_”
} 
```
- Response :
    - Error : 
    ```
    {
        code: CODE_ERROR,
        message: "Error : " + _error_
    }
    ```
    - Success : 
    ```
    {
        code: CODE_SUCCESS,
        message: "Success"
    }
    ```
### Status
- Description : Set the status of a specific user
- Type : POST
- Address : ```http://ip:port/status/```
- Payload : 
```
{
	“token”: “_user_token”
	“navigating”: “true|false”
} 
 
```
- Response :
    - Error : 
    ```
    {
        code: CODE_ERROR,
        message: "Error : " + _error_
    }
    ```
    - Success : 
    ```
    {
        code: CODE_SUCCESS,
        message: "Success"
    }
    ```
### Request Location
- Description : Request to user who are navigating their location
- Time : REQUEST_TIMEOUT_LOCATION
- Type : FCM
- Message payload : 
```
{
    data: {
        type: "location"
    },
    tokens: registrationTokens
}
```
### Request Nav
- Description : Request to all user who are not navigating if they are navigating
- Time : REQUEST_TIMEOUT_ISNAV
- Type : FCM
- Message payload : 
```
{
    data: {
        type: "status"
    },
    tokens: registrationTokens
}
```