const log = require("./log.js");
module.exports = function(res, req, usersRef) {
  if (req.body.token == "") {
    res.json({
      code: process.env.CODE_ERROR,
      message: "Token must be not null"
    });
  } else {
    usersRef
      .doc(req.body.token)
      .update({ navigating: req.body.navigating })
      .catch(error => {
        // If there is an error while updating the document, return message error
        res.json({
          code: process.env.CODE_ERROR,
          message: "Error : " + error
        });
        log.error("Error : ", error);
      })
      .then(() => {
        // Send message success when user updated
        res.json({
          code: process.env.CODE_SUCCESS,
          message: "success"
        });
        log.info(
          "Successfully updated status to user :",
          req.body.token
        );
      });
  }
};
