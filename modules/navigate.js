const tomtom = require("@tomtom-international/web-sdk-services/dist/services-node.min.js");
const log = require("./log.js");
// TODO: improve the way we currently detect, if a new route
// needs to be sent!
module.exports = function(res,req,admin) {
    const speed = req.body.currentSpeed;
    const params = req.body;
    log.info("NAVIGATE", `Receive data from ${params.userFirebaseToken}`);
    if (speed >= 20 && speed <= 30) {
      const options = buildTomTomSearchParams(params);
  
      tomtom.services
        .calculateRoute(options)
        .go()
        .then(result => {
          const gParams = buildGMapParams(result.routes[0], params.distCoord);
  
          const gURL =
            "https://www.google.com/maps/dir/?api=1" +
            `&destination=${gParams.Destination}` +
            `&travelmode=${gParams.TravelMode}` +
            `&waypoints=${gParams.Waypoints}`;
  
          log.debug(gURL);
  
          const message = {
            data: {
              type: "url",
              message: gURL
            },
            token: params.userFirebaseToken
          };
  
          admin
            .messaging()
            .send(message)
            .then(response => {
              log.info("Successfully sent message: ", response);
              res.json({
                code: process.env.CODE_SUCCESS,
                message: "Success"
              });
            })
            .catch(error => {
              log.error("Error sending message: ", error);
            });
        })
        .catch(error => {
          log.error("Error calculating the new route: ", error);
        });
    } else {
      //TODO: correct response if speed not in threshold
      res.json({
        code: process.env.CODE_SUCCESS,
        message: "Speed not in threshold"
      });
    }
}

/*
 * Returns either true if @param is null/undefined or
 * false is parameter is set
 */
function isNullOrUndifined(param) {
    if (param == null || param == undefined) {
      return true;
    }
    return false;
  }
  function buildTomTomSearchParams(params) {
    const locationString = `${params.sourceCoord.lon},${params.sourceCoord.lat}:${params.distCoord.lon},${params.distCoord.lat}`;
    var avoids = ["tollRoads", "ferries"];
    var tMode = "car";
    if (!isNullOrUndifined(params.avoidHighways) && params.avoidHighways) {
      avoids.push("motorways");
    }
    if (!isNullOrUndifined(params.isTruck) && params.isTruck) {
      tMode = "truck";
    }
  
    const options = {
      key: process.env.TOMTOM_API_KEY,
      locations: locationString,
      alternativType: "betterRoute",
      avoid: avoids,
      routeType: "fastest",
      traffic: true,
      travelMode: tMode,
      vehicleWidth: isNullOrUndifined(params.vehicleWidth)
        ? 0
        : params.vehicleWidth,
      vehicleHeight: isNullOrUndifined(params.vehicleHeight)
        ? 0
        : params.vehicleHeight,
      vehicleLength: isNullOrUndifined(params.vehicleLength)
        ? 0
        : params.vehicleLength,
      vehicleWeight: isNullOrUndifined(params.vehicleWeight)
        ? 0
        : params.vehicleWeight
    };
  
    return options;
  }
  
  function buildGMapParams(route, destCoords) {
    var result = {};
  
    const pointCount = route.sections[0].endPointIndex;
    const waypointDelta = Math.floor(pointCount / 7);
  
    result.Destination = `${destCoords.lat},${destCoords.lon}`;
    result.TravelMode = "driving";
    result.Waypoints = "";
    for (var i = 0; i < pointCount; i += waypointDelta) {
      if (i > 0) {
        result.Waypoints += "|";
      }
      result.Waypoints += `${route.legs[0].points[i].lat},${route.legs[0].points[i].lng}`;
    }
  
    return result;
  }