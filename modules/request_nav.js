const log = require("./log.js");
module.exports = function(usersRef, admin) {
  //log.info("REQUEST NAV");
  const registrationTokens = [];
  // Get all users
  usersRef
    .get()
    .then(snapshot => {
      // Loop into users
      snapshot.forEach(doc => {
        // If users is not navigating
        if (!doc.data().navigating) {
          // Add the user token to the array
          registrationTokens.push(doc.id);
        }
      });
      // If all users are navigating, abort
      if (registrationTokens.length == 0) return;
      // Define the message for request isnav
      const message = {
        data: {
          type: "status"
        },
        tokens: registrationTokens
      };
      //log(LOG_INFO, "Message : ", message);
      // Send the message to all user who are not navigating
      admin
        .messaging()
        .sendMulticast(message)
        .catch(error => {
          // If there is an error while sending the messages
          log.error("Error sending message:", error);
        })
        .then(response => {
          // Log when message successfully sent
          //log(LOG_INFO, response.successCount + " messages were sent successfully");
        });
    })
    .catch(err => {
      // If there is an error while getting all users
      log.error("Error getting documents", err);
    });
};
