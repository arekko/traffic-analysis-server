const LOG_INFO = "INFO LOG";
const LOG_ERROR = "INFO ERROR";
const LOG_DEBUG = "INFO DEBUG";
var log = {
    info: function (message, param = "") { 
        console.log(`# ${new Date().toLocaleString('en-GB', { timeZone: 'Europe/Helsinki' })} - ${LOG_INFO} : ${message}`, param);
    },
    debug:function (message, param = "") { 
        console.log(`# ${new Date().toLocaleString('en-GB', { timeZone: 'Europe/Helsinki' })} - ${LOG_DEBUG} : ${message}`, param);
    },
    error:function (message, param = "") { 
        console.log(`# ${new Date().toLocaleString('en-GB', { timeZone: 'Europe/Helsinki' })} - ${LOG_ERROR} : ${message}`, param);
    }
};



module.exports = log;