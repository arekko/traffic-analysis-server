const admin = require("firebase-admin");
const serviceAccount = require(process.env.PATH_FIREBASE_FILE);
module.exports = () => {
  // Firebase admin sdk initialization
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://traffium-7d102.firebaseio.com"
  });
  return admin;
};
