const log = require("./log.js");
module.exports = function(res, req, usersRef) {
  const body = req.body;
  if (body.registrationToken == "") {
    res.json({
      code: process.env.CODE_ERROR,
      message: "Token must be not null"
    });
  } else {
    log.info("REGISTER | User register : " + body.registrationToken);
    // Create a new document for the user identidied by his token
    usersRef
      .doc(body.registrationToken)
      .set({
        navigating: false
      })
      .catch(error => {
        // If there is an error while setting the document, return json code error message
        res.json({
          code: process.env.CODE_ERROR,
          message: "Error : " + error
        });
        log.error("Error : ", error);
      })
      .then(() => {
        // Send message success when user added to db
        res.json({
          code: process.env.CODE_SUCCESS,
          message: "success"
        });
        log.info("Successfully added user to db :", body.registrationToken);
      });
  }
};
