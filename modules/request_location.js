const log = require("./log.js");
module.exports = function(usersRef, admin) {
  log.info("REQUEST LOCATION");
  const registrationTokens = [];
  // Get all users
  usersRef
    .get()
    .then(snapshot => {
      // Loop into users
      snapshot.forEach(doc => {
        // If the user is navigating
        if (doc.data().navigating) {
          // We add his token to the array
          registrationTokens.push(doc.id.toString());
        }
      });
      // If no users are navigating, abort
      if (registrationTokens.length == 0) return;
      // Define the message to send
      const message = {
        data: {
          type: "location"
        },
        tokens: registrationTokens
      };
      //log(LOG_INFO, "Message : ", message);
      // Send the message to all users in th array
      admin
        .messaging()
        .sendMulticast(message)
        .catch(error => {
          // If there is an error, log it
          log.error("Error sending message:", error);
        })
        .then(response => {
          // Log when message successfully sent
          //log(LOG_INFO, response.successCount + " messages were sent successfully");
        });
    })
    .catch(err => {
      // If there is an error while getting all users
      log.error("Error getting documents : ", err);
    });
};
